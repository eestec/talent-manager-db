-- Step 3. Load the skills into the DB 

BEGIN TRANSACTION;

INSERT INTO skill (skill_name, skill_type) VALUES

-- Hard Skills

-- General

('GSuite Tools', 'hard_skill'),
('Social Media Post Creation', 'hard_skill'),
('Guides and Reports Creation', 'hard_skill'),

-- Design

('Adobe Photoshop', 'hard_skill'),
('Adobe Illustrator', 'hard_skill'),
('Adobe InDesign', 'hard_skill'),
('Adobe XD', 'hard_skill'),
('Video Animation & Video Editing', 'hard_skill'),
('UX/UI', 'hard_skill'),
('Logo design or Illustrations', 'hard_skill'),
('Branding/EESTEC Brand', 'hard_skill'),
('Basic Design Principles', 'hard_skill'),

-- IT

('HTML & CSS', 'hard_skill'),
('WordPress', 'hard_skill'),
('Laravel & PHP', 'hard_skill'),
('Git', 'hard_skill'),
('React', 'hard_skill'),
('Relational Databases (MySQL & PostgreSQL)', 'hard_skill'),
('Python', 'hard_skill'),
('Linux OS', 'hard_skill'),
('Docker', 'hard_skill'),


-- Soft Skills

-- Organizational Management

('Knowledge Transfer', 'soft_skill'), 
('Project Management (PM)', 'soft_skill'),

-- Intrapersonal Skills

('Goal Setting', 'soft_skill'),
('Stress Management', 'soft_skill'),
('Presentation Skills', 'soft_skill'),
('Personal Motivation', 'soft_skill'),

-- Interpersonal Skills

('Communication Skills', 'soft_skill'),
('Conflict Management', 'soft_skill'),
('Facilitation', 'soft_skill'),
('Feedback', 'soft_skill'),
('Decision Making', 'soft_skill'),
('Group Dynamics / Teamwork', 'soft_skill'),
('Negotiation / Persuasion skills', 'soft_skill'),
('Problem Solving', 'soft_skill'),
('Team Management', 'soft_skill'),
('Time Management', 'soft_skill'),


-- Personality Traits

('Proactive', 'personality_trait'),
('Team Player', 'personality_trait'),
('Motivating', 'personality_trait'),
('Creative', 'personality_trait'),
('Confident', 'personality_trait'),
('Organized', 'personality_trait'),
('Open-minded', 'personality_trait'),
('Empathetic', 'personality_trait'),
('Open to feedback/Willing to change', 'personality_trait'),
('Having integrity/Transparent', 'personality_trait'),
('Supportive', 'personality_trait'),
('Patient', 'personality_trait'),
('Disciplined', 'personality_trait'),
('Approachable', 'personality_trait'),
('Determined', 'personality_trait'),
('Accountable', 'personality_trait'),
('Inspiring', 'personality_trait'),
('Adaptable', 'personality_trait'),
('Curious', 'personality_trait'),
('Social-Outgoing', 'personality_trait'),
('Responsive', 'personality_trait'),
('Analytical', 'personality_trait'),
('Passionate', 'personality_trait'),
('Goal-oriented', 'personality_trait'),
('Consistent ', 'personality_trait'),
('Resilient/Flexible', 'personality_trait'),
('Energetic', 'personality_trait'),
('Leading by example', 'personality_trait'),
('Efficient', 'personality_trait')

-- COMMIT --ROLLBACK
------------------------------------------------------------------------------
------------------------------------------------------------------------------

-- Step 4. Load the positions into the DB

BEGIN TRANSACTION;

INSERT INTO position (position_type, position_title) VALUES

('Board Assistant',	'Ambassador'),
('Board Assistant',	'New Technological Project Board Assistant'),
('Board Assistant',	'Alumni Relations Manager'),
('Coordinator',	'HEAT International Awareness Team Coordinator'),
('Coordinator',	'PR Team Coordinator for Education'),
('Coordinator',	'Training Team Internal Affairs Coordinator'),
('Coordinator',	'Data Team History Collectors Team Coordinator'),
('Coordinator',	'Design Team Coordinator for Workflow'),
('Coordinator',	'CR Team Projects’ Coordinator'),
('Coordinator',	'PR Team Designer'),
('Coordinator',	'PR Team Digital Coordinator'),
('Coordinator',	'Publications Project Content Team Coordinator'),
('Coordinator',	'Regionalization Project Region I Coordinator'),
('Coordinator',	'IT Team Projects Team Coordinator'),
('Coordinator',	'Design Team Coordinator for Education'),
('Coordinator',	'SSA Project Internal Affairs Coordinator'),
('Coordinator',	'EC Project Designer'),
('Coordinator',	'Regionalization Project Region IV Coordinator'),
('Coordinator',	'EC Project PR Team'),
('Coordinator',	'IT Team Admins Team Coordinator'),
('Coordinator',	'Design Team Coordinator for Video Technologies'),
('Coordinator',	'Training Team External Affairs Coordinator'),
('Coordinator',	'Regionalization Project Educators Coordinator'),
('Coordinator',	'EC Projects Academics Team Coordinator'),
('Coordinator',	'Publications Project PR Team Coordinator'),
('Coordinator',	'CR Team Corporate Relations Coordinator'),
('Coordinator',	'PR Team Branding Manager'),
('Coordinator',	'IT Team Knowledge & Education Team Coordinator'),
('Coordinator',	'Data Team Statistics Team Coordinator'),
('Coordinator',	'Regionalization Project Region V Coordinator'),
('Coordinator',	'SSA Project Designer'),
('Coordinator',	'PR Team Content Coordinator'),
('Coordinator',	'Data Team Event Controllers Team Coordinator'),
('Coordinator',	'EC Project Ambassadors Team Coordinator'),
('Coordinator',	'HEAT Online poWered Learning Team Coordinator'),
('Coordinator',	'SSA Project External Affairs Coordinator'),
('Coordinator',	'Publications Project Designer'),
('Coordinator',	'Regionalization Project Angels Coordinator'),
('Coordinator',	'HEAT Talent Manager'),
('Leader',	'Publications Project Leader'),
('Leader',	'IT Team Leader'),
('Leader',	'HEAT Leader'),
('Leader',	'Regionalization Project Leader'),
('Leader',	'Training Team Leader'),
('Leader',	'PR Team Leader'),
('Leader',	'Design Team Leader'),
('Leader',	'EC Project Leader'),
('Leader',	'SSA Project Leader'),
('member of the Board of the Association',	'Chairperson'),
('member of the Board of the Association',	'Vice Chairperson for External Affairs'),
('member of the Board of the Association',	'Treasurer'),
('member of the Board of the Association',	'Vice Chairperson for Administrative Affairs'),
('member of the Board of the Association',	'Vice Chairperson for Internal Affairs')

-- COMMIT --ROLLBACK