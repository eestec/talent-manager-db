-- Step 7. Insert Poeple's answers to main tables

BEGIN TRANSACTION;

CREATE FUNCTION import_people()
RETURNS VOID
LANGUAGE plpgsql
AS $$

DECLARE per_email VARCHAR(50);
DECLARE per_id INT;
DECLARE skl_id INT;
DECLARE rate INT;

BEGIN

LOOP

SELECT email INTO per_email
    FROM peoples_answers
        FETCH FIRST 1 ROWS ONLY;

IF per_email IS NULL THEN 
    exit;
END IF;

SELECT person_id    INTO per_id FROM person             WHERE email = per_email;

SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='GSuite Tools';
SELECT GSuite_Tools INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Social Media Post Creation';
SELECT Social_Media_Post_Creation             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Guides and Reports Creation';
SELECT Guides_and_Reports_Creation             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe Photoshop';
SELECT Adobe_Photoshop             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe Illustrator';
SELECT Adobe_Illustrator             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe InDesign';
SELECT Adobe_InDesign             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe XD';
SELECT Adobe_XD             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Video Animation & Video Editing';
SELECT Video_Animation_Video_Editing             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='UX/UI';
SELECT UX_UI             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Logo design or Illustrations';
SELECT Logo_design_or_Illustrations             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Branding/EESTEC Brand';
SELECT Branding_EESTEC_Brand             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Basic Design Principles';
SELECT Basic_Design_Principles             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='HTML & CSS';
SELECT HTML_CSS             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='WordPress';
SELECT WordPress             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Laravel & PHP';
SELECT Laravel_PHP             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Git';
SELECT Git             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='React';
SELECT React             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Relational Databases (MySQL & PostgreSQL)';
SELECT Relational_Databases             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Python';
SELECT Python             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Linux OS';
SELECT Linux_OS             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Docker';
SELECT Docker             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Knowledge Transfer';
SELECT Knowledge_Transfer             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Project Management (PM)';
SELECT Project_Management             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Goal Setting';
SELECT Goal_Setting             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Stress Management';
SELECT Stress_Management             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Presentation Skills';
SELECT Presentation_Skills             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Personal Motivation';
SELECT Personal_Motivation             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Communication Skills';
SELECT Communication_Skills             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Conflict Management';
SELECT Conflict_Management             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Facilitation';
SELECT Facilitation             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Feedback';
SELECT Feedback             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Decision Making';
SELECT Decision_Making             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Group Dynamics / Teamwork';
SELECT Group_Dynamics_Teamwork             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Negotiation / Persuasion skills';
SELECT Negotiation_Persuasion_skills             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Problem Solving';
SELECT Problem_Solving             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Team Management';
SELECT Team_Management             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Time Management';
SELECT Time_Management             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Proactive';
SELECT Proactive             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Team Player';
SELECT Team_Player             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Motivating';
SELECT Motivating             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Creative';
SELECT Creative             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Confident';
SELECT Confident             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Organized';
SELECT Organized             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Open-minded';
SELECT Open_minded             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Empathetic';
SELECT Empathetic             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Open to feedback/Willing to change';
SELECT Open_to_feedback_Willing_to_change             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Having integrity/Transparent';
SELECT Having_integrity_Transparent             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Supportive';
SELECT Supportive             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Patient';
SELECT Patient             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Disciplined';
SELECT Disciplined             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Approachable';
SELECT Approachable             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Determined';
SELECT Determined             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Accountable';
SELECT Accountable             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Inspiring';
SELECT Inspiring             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adaptable';
SELECT Adaptable             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Curious';
SELECT Curious             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Social-Outgoing';
SELECT Social_Outgoing             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Responsive';
SELECT Responsive             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Analytical';
SELECT Analytical             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Passionate';
SELECT Passionate             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Goal-oriented';
SELECT Goal_oriented             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Consistent ';
SELECT Consistent             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Resilient/Flexible';
SELECT Resilient_Flexible             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Energetic';
SELECT Energetic             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Leading by example';
SELECT Leading_by_example             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Efficient';
SELECT Efficient             INTO rate   FROM peoples_answers    WHERE email = per_email;

INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (per_id, skl_id, rate, 'Person');

DELETE FROM peoples_answers WHERE email = per_email;

END LOOP;

END; $$
-- COMMIT --ROLLBACK

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-- Step 8. Insert Positions' answers to main tables

BEGIN TRANSACTION;

CREATE FUNCTION import_positions()
RETURNS VOID
LANGUAGE plpgsql
AS $$

DECLARE pos VARCHAR(50);
DECLARE pos_id INT;
DECLARE skl_id INT;
DECLARE rate INT;

BEGIN

LOOP

SELECT position_title INTO pos
    FROM positions_answers
        FETCH FIRST 1 ROWS ONLY;

IF pos IS NULL THEN 
    exit;
END IF;

SELECT position_id    INTO pos_id FROM position             WHERE position_title = pos;

SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='GSuite Tools';
SELECT GSuite_Tools INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Social Media Post Creation';
SELECT Social_Media_Post_Creation             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Guides and Reports Creation';
SELECT Guides_and_Reports_Creation             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe Photoshop';
SELECT Adobe_Photoshop             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe Illustrator';
SELECT Adobe_Illustrator             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe InDesign';
SELECT Adobe_InDesign             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe XD';
SELECT Adobe_XD             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Video Animation & Video Editing';
SELECT Video_Animation_Video_Editing             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='UX/UI';
SELECT UX_UI             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Logo design or Illustrations';
SELECT Logo_design_or_Illustrations             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Branding/EESTEC Brand';
SELECT Branding_EESTEC_Brand             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Basic Design Principles';
SELECT Basic_Design_Principles             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='HTML & CSS';
SELECT HTML_CSS             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='WordPress';
SELECT WordPress             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Laravel & PHP';
SELECT Laravel_PHP             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Git';
SELECT Git             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='React';
SELECT React             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Relational Databases (MySQL & PostgreSQL)';
SELECT Relational_Databases             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Python';
SELECT Python             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Linux OS';
SELECT Linux_OS             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Docker';
SELECT Docker             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Knowledge Transfer';
SELECT Knowledge_Transfer             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Project Management (PM)';
SELECT Project_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Goal Setting';
SELECT Goal_Setting             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Stress Management';
SELECT Stress_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Presentation Skills';
SELECT Presentation_Skills             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Personal Motivation';
SELECT Personal_Motivation             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Communication Skills';
SELECT Communication_Skills             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Conflict Management';
SELECT Conflict_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Facilitation';
SELECT Facilitation             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Feedback';
SELECT Feedback             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Decision Making';
SELECT Decision_Making             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Group Dynamics / Teamwork';
SELECT Group_Dynamics_Teamwork             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Negotiation / Persuasion skills';
SELECT Negotiation_Persuasion_skills             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Problem Solving';
SELECT Problem_Solving             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Team Management';
SELECT Team_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Time Management';
SELECT Time_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Proactive';
SELECT Proactive             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Team Player';
SELECT Team_Player             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Motivating';
SELECT Motivating             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Creative';
SELECT Creative             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Confident';
SELECT Confident             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Organized';
SELECT Organized             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Open-minded';
SELECT Open_minded             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Empathetic';
SELECT Empathetic             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Open to feedback/Willing to change';
SELECT Open_to_feedback_Willing_to_change             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Having integrity/Transparent';
SELECT Having_integrity_Transparent             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Supportive';
SELECT Supportive             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Patient';
SELECT Patient             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Disciplined';
SELECT Disciplined             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Approachable';
SELECT Approachable             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Determined';
SELECT Determined             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Accountable';
SELECT Accountable             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Inspiring';
SELECT Inspiring             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adaptable';
SELECT Adaptable             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Curious';
SELECT Curious             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Social-Outgoing';
SELECT Social_Outgoing             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Responsive';
SELECT Responsive             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Analytical';
SELECT Analytical             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Passionate';
SELECT Passionate             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Goal-oriented';
SELECT Goal_oriented             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Consistent ';
SELECT Consistent             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Resilient/Flexible';
SELECT Resilient_Flexible             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Energetic';
SELECT Energetic             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Leading by example';
SELECT Leading_by_example             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Efficient';
SELECT Efficient             INTO rate   FROM positions_answers    WHERE position_title = pos;

    INSERT INTO matching (entry_id, skill_id, rating, entry_type) VALUES
    (pos_id, skl_id, rate, 'Position');

DELETE FROM positions_answers WHERE position_title = pos;

END LOOP;

END; $$
-- COMMIT --ROLLBACK

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-- Step 9. Fuction for getting the AVG of Position ratings 

BEGIN TRANSACTION;

CREATE FUNCTION import_AVG_positions()
RETURNS VOID
LANGUAGE plpgsql
AS $$

DECLARE pos VARCHAR(50);
DECLARE pos_id INT;
DECLARE skl_id INT;
DECLARE rate INT;

BEGIN

LOOP

SELECT position_title INTO pos
    FROM positions_answers
        FETCH FIRST 1 ROWS ONLY;

IF pos IS NULL THEN 
    exit;
END IF;

SELECT position_id    INTO pos_id FROM position             WHERE position_title = pos;

SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='GSuite Tools';
SELECT GSuite_Tools INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
        FROM matching 
        WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Social Media Post Creation';
SELECT Social_Media_Post_Creation             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Guides and Reports Creation';
SELECT Guides_and_Reports_Creation             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe Photoshop';
SELECT Adobe_Photoshop             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe Illustrator';
SELECT Adobe_Illustrator             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe InDesign';
SELECT Adobe_InDesign             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adobe XD';
SELECT Adobe_XD             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Video Animation & Video Editing';
SELECT Video_Animation_Video_Editing             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='UX/UI';
SELECT UX_UI             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Logo design or Illustrations';
SELECT Logo_design_or_Illustrations             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Branding/EESTEC Brand';
SELECT Branding_EESTEC_Brand             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Basic Design Principles';
SELECT Basic_Design_Principles             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='HTML & CSS';
SELECT HTML_CSS             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='WordPress';
SELECT WordPress             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Laravel & PHP';
SELECT Laravel_PHP             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Git';
SELECT Git             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='React';
SELECT React             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Relational Databases (MySQL & PostgreSQL)';
SELECT Relational_Databases             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Python';
SELECT Python             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Linux OS';
SELECT Linux_OS             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Docker';
SELECT Docker             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Knowledge Transfer';
SELECT Knowledge_Transfer             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Project Management (PM)';
SELECT Project_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Goal Setting';
SELECT Goal_Setting             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Stress Management';
SELECT Stress_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Presentation Skills';
SELECT Presentation_Skills             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Personal Motivation';
SELECT Personal_Motivation             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Communication Skills';
SELECT Communication_Skills             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Conflict Management';
SELECT Conflict_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Facilitation';
SELECT Facilitation             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Feedback';
SELECT Feedback             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Decision Making';
SELECT Decision_Making             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Group Dynamics / Teamwork';
SELECT Group_Dynamics_Teamwork             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Negotiation / Persuasion skills';
SELECT Negotiation_Persuasion_skills             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Problem Solving';
SELECT Problem_Solving             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Team Management';
SELECT Team_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Time Management';
SELECT Time_Management             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Proactive';
SELECT Proactive             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Team Player';
SELECT Team_Player             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Motivating';
SELECT Motivating             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Creative';
SELECT Creative             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Confident';
SELECT Confident             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Organized';
SELECT Organized             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Open-minded';
SELECT Open_minded             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Empathetic';
SELECT Empathetic             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Open to feedback/Willing to change';
SELECT Open_to_feedback_Willing_to_change             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Having integrity/Transparent';
SELECT Having_integrity_Transparent             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Supportive';
SELECT Supportive             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Patient';
SELECT Patient             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Disciplined';
SELECT Disciplined             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Approachable';
SELECT Approachable             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Determined';
SELECT Determined             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Accountable';
SELECT Accountable             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Inspiring';
SELECT Inspiring             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Adaptable';
SELECT Adaptable             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Curious';
SELECT Curious             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Social-Outgoing';
SELECT Social_Outgoing             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Responsive';
SELECT Responsive             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Analytical';
SELECT Analytical             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Passionate';
SELECT Passionate             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Goal-oriented';
SELECT Goal_oriented             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Consistent ';
SELECT Consistent             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Resilient/Flexible';
SELECT Resilient_Flexible             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Energetic';
SELECT Energetic             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Leading by example';
SELECT Leading_by_example             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';


SELECT skill_id     INTO skl_id FROM skill              WHERE skill_name ='Efficient';
SELECT Efficient             INTO rate   FROM positions_answers    WHERE position_title = pos;

SELECT (rating + rate)/2 INTO rate 
    FROM matching 
    WHERE skill_id = skl_id 
        AND entry_id = pos_id 
        AND entry_type = 'Position'; 

UPDATE matching
SET rating = rate
WHERE skill_id = skl_id 
    AND entry_id = pos_id 
    AND entry_type = 'Position';

DELETE FROM positions_answers WHERE position_title = pos;

END LOOP;

END; $$
-- COMMIT --ROLLBACK